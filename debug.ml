open Td;;

let debug s = output_string stderr s; flush stderr;;

let debug_char c = output_char stderr c; flush stderr;;

let log s = output_string stdout s;;

let log_char s = output_char stdout s;;

let char_of_tile = function
  | `Rook, 1 -> 'r'
  | `Knight, 1 -> 'n'
  | `Bishop, 1 -> 'b'
  | `Queen, 1 -> 'q'
  | `King, 1 -> 'k'
  | `Pawn, 1 -> 'p'
  | `Rook, 2 -> 'R'
  | `Knight, 2 -> 'N'
  | `Bishop, 2 -> 'B'
  | `Queen, 2 -> 'Q'
  | `King, 2 -> 'K'
  | `Pawn, 2 -> 'P'
  | `Empty, 0 -> '.'
  | _ -> '.'
;;

let string_of_tile = function
  | `Rook, 1 -> "r"
  | `Knight, 1 -> "n"
  | `Bishop, 1 -> "b"
  | `Queen, 1 -> "q"
  | `King, 1 -> "k"
  | `Pawn, 1 -> "p"
  | `Rook, 2 -> "R"
  | `Knight, 2 -> "N"
  | `Bishop, 2 -> "B"
  | `Queen, 2 -> "Q"
  | `King, 2 -> "K"
  | `Pawn, 2 -> "P"
  | `Empty, 0 -> "."
  | _ -> "."
;;

let log_grid grid =
  log_char '\n';
  Array.iter (fun row -> Array.iter (
      fun t -> log_char (char_of_tile t)
  ) row; log_char '\n') grid;
  log_char '\n';
  log_char '\n';
;;

let log_orders l =
  List.iter (fun ((sr, sc), (tr, tc)) -> 
    log (Printf.sprintf "%d %d to %d %d\n" sr sc tr tc)
  ) l
;;

let flush_log () = flush stdout;;
