This is a game simulator for hackerrank's Antichess challenge.

To build:

ocamlbuild playgame.native

To run:

./playgame.native "bot command for player 1" "bot command for player 2"

Bug reports and feedback welcome.

