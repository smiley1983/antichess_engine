open Td;;

let bot_stderr_file bot =
  let name = Printf.sprintf "%d" bot.id in
    "logs/player_" ^ name ^ "_stderr.log"
;;

let run_bot game bot =
  let state = Game.render game bot in
  let stdin_in, stdin_out = Unix.pipe () in
  let stdout_in, stdout_out = Unix.pipe () in
(*
  let in_chan, out_chan = Unix.open_process (bot.Game.cmd ^ " 2>err.txt") in
*)
  let bot_stderr = Unix.descr_of_out_channel (open_out (bot_stderr_file bot)) in
  let pid = Unix.create_process bot.cmd [||] stdin_in stdout_out bot_stderr in
  let out_chan = Unix.out_channel_of_descr stdin_out in
  let in_chan = Unix.in_channel_of_descr stdout_in in
    output_string out_chan state;
    flush out_chan;
    let order = Game.read_order in_chan in (*use non-blocking and check time*)
      Unix.kill pid 9;
      Unix.close bot_stderr;
      Unix.close stdin_in;
      Unix.close stdin_out;
      Unix.close stdout_in;
      Unix.close stdout_out;
      order
;;

let run_game game =
  while not (Game.finished game.model) do
(*
    let orders = List.map 
      (fun bot -> bot.Game.color, (run_bot game.model bot)) game.players 
    in
*)
    let player = 
      if game.model.turn mod 2 = 0 then game.player2 else game.player1 
    in
    let order = (player.id, (run_bot game.model player)) in
(*      Game.log game.model orders; *)
      Game.update game.model order;
      Debug.log (Printf.sprintf "turn = %d\n" game.model.turn);
      Debug.log_grid game.model.grid;
      flush stderr;
      flush stdout;
      game.model.turn <- game.model.turn + 1;
  done
;;

let init bot_cmd1 bot_cmd2 =
  let player1 = Game.new_player bot_cmd1 1 in
  let player2 = Game.new_player bot_cmd2 2 in
  let m = Game.new_state () in
  let g =
   {
    player1 = player1;
    player2 = player2;
    model = m;
   }
  in
    run_game g;
    let s1, s2 = Game.score_game g.model in
    Debug.log (Printf.sprintf "final score:\n player 1: %d\n player 2: %d\n" (-s1) (-s2));
;;

let () =
  try
    init Sys.argv.(1) Sys.argv.(2)
  with e -> failwith (Printf.sprintf "Failed with exception %s \n\n usage: \n  playgame bot_cmd1 bot_cmd2\n\n" (Printexc.to_string e))
;;

